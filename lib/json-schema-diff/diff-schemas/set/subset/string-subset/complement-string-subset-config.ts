import {defaultMaxLength, defaultMinLength} from '../../keyword-defaults';
import {StringSubsetConfig} from './string-subset-config';

const complementMaxLength = (config: StringSubsetConfig): StringSubsetConfig => ({
    maxLength: defaultMaxLength,
    minLength: config.maxLength + 1
});

const complementMinLength = (config: StringSubsetConfig): StringSubsetConfig => ({
    maxLength: config.minLength - 1,
    minLength: defaultMinLength
});

export const complementStringSubsetConfig = (config: StringSubsetConfig): StringSubsetConfig[] =>
    [
        complementMaxLength(config),
        complementMinLength(config)
    ];
