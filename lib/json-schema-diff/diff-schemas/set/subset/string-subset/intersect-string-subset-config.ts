import {StringSubsetConfig} from './string-subset-config';

const intersectMaxLength = (configA: StringSubsetConfig, configB: StringSubsetConfig): number =>
    Math.min(configA.maxLength, configB.maxLength);

const intersectMinLength = (configA: StringSubsetConfig, configB: StringSubsetConfig): number =>
    Math.max(configA.minLength, configB.minLength);

export const intersectStringSubsetConfig = (
    configA: StringSubsetConfig,
    configB: StringSubsetConfig
): StringSubsetConfig => ({
    maxLength: intersectMaxLength(configA, configB),
    minLength: intersectMinLength(configA, configB)
});
