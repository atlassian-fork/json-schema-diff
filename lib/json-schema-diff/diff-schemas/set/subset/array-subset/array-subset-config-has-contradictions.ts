import {ArraySubsetConfig} from './array-subset-config';

const isEmptyItemsAndMinItemsContradiction = (config: ArraySubsetConfig): boolean => {
    const itemsAcceptsNoValues = config.items.type === 'empty';
    const minItemsRequiresValues = config.minItems > 0;
    return itemsAcceptsNoValues && minItemsRequiresValues;
};

const isMaxItemsAndMinItemsContradiction = (config: ArraySubsetConfig): boolean => config.minItems > config.maxItems;

const isMinItemsContradiction = (config: ArraySubsetConfig): boolean => config.minItems === Infinity;

const isItemsAndNotItemsContradiction = (config: ArraySubsetConfig): boolean =>
    config.not.some((notConfig) => config.items.isSubsetOf(notConfig.items));

type contradictionTestFn = (config: ArraySubsetConfig) => boolean;

const contradictionTests: contradictionTestFn[] = [
    isEmptyItemsAndMinItemsContradiction,
    isMaxItemsAndMinItemsContradiction,
    isMinItemsContradiction,
    isItemsAndNotItemsContradiction
];

export const arraySubsetConfigHasContradictions = (config: ArraySubsetConfig): boolean =>
    contradictionTests.some((contradictionTest) => contradictionTest(config));
