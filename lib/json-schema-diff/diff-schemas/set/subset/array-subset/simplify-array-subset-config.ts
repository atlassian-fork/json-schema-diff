import {emptyJsonSet} from '../../json-set';
import {defaultMaxItems} from '../../keyword-defaults';
import {ArraySubsetConfig} from './array-subset-config';

const maxItemsAllowsNoItems = (config: ArraySubsetConfig): boolean => config.maxItems === 0;

/*
All ArraySubsetConfig objects go through this simplification when being created. The only way 2 or more entries can
appear inside the not array is when two existing ArraySubsetConfig are intersected, both of which are simplified
already. Therefore we only need to apply this simplification when config.not === 1.
*/
const notItemsDisallowsEmptyArrays = (config: ArraySubsetConfig): boolean =>
    config.not.length === 1 && config.not[0].items.type === 'empty';

export const simplifyArraySubsetConfig = (config: ArraySubsetConfig): ArraySubsetConfig => {
    if (maxItemsAllowsNoItems(config)) {
        return {...config, items: emptyJsonSet, maxItems: defaultMaxItems};
    }

    if (notItemsDisallowsEmptyArrays(config)) {
        return {...config, not: [], minItems: 1};
    }

    return config;
};
