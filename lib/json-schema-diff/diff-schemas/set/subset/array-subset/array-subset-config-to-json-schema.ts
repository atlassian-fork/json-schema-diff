import {CoreSchemaMetaSchema, JsonSchema} from 'json-schema-spec-types';
import {defaultMaxItems, defaultMinItems} from '../../keyword-defaults';
import {addAnyOfIfNecessary} from '../../set';
import {ArraySubsetConfig} from './array-subset-config';

const getNotSchema = (config: ArraySubsetConfig): CoreSchemaMetaSchema =>
    config.not.length === 0 ? {} : {not: addAnyOfIfNecessary(config.not.map(arraySubsetConfigToJsonSchema))};

const getItemsSchema = (config: ArraySubsetConfig): CoreSchemaMetaSchema =>
    config.items.type === 'all' ? {} : {items: config.items.toJsonSchema()};

const getMaxItemsSchema = (config: ArraySubsetConfig): CoreSchemaMetaSchema =>
    config.maxItems === defaultMaxItems ? {} : {maxItems: config.maxItems};

const getMinItemsSchema = (config: ArraySubsetConfig): CoreSchemaMetaSchema =>
    config.minItems === defaultMinItems ? {} : {minItems: config.minItems};

export const arraySubsetConfigToJsonSchema = (config: ArraySubsetConfig): JsonSchema => ({
    type: 'array',
    ...getItemsSchema(config),
    ...getMaxItemsSchema(config),
    ...getMinItemsSchema(config),
    ...getNotSchema(config)
});
