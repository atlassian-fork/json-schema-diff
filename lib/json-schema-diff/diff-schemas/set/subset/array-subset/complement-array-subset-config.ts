import {allJsonSet} from '../../json-set';
import {defaultMaxItems, defaultMinItems} from '../../keyword-defaults';
import {ArraySubsetConfig} from './array-subset-config';

const complementItems = (config: ArraySubsetConfig): ArraySubsetConfig => ({
    items: allJsonSet,
    maxItems: defaultMaxItems,
    minItems: defaultMinItems,
    not: [{
        items: config.items,
        maxItems: defaultMaxItems,
        minItems: defaultMinItems,
        not: []
    }]
});

const complementMaxItems = (config: ArraySubsetConfig): ArraySubsetConfig => ({
    items: allJsonSet,
    maxItems: defaultMaxItems,
    minItems: config.maxItems + 1,
    not: []
});

const complementMinItems = (config: ArraySubsetConfig): ArraySubsetConfig => ({
    items: allJsonSet,
    maxItems: config.minItems - 1,
    minItems: defaultMinItems,
    not: []
});

const complementNot = (config: ArraySubsetConfig): ArraySubsetConfig[] => config.not;

export const complementArraySubsetConfig = (config: ArraySubsetConfig): ArraySubsetConfig[] =>
    [
        complementItems(config),
        complementMaxItems(config),
        complementMinItems(config),
        ...complementNot(config)
    ];
