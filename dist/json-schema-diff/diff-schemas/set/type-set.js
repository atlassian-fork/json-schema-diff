"use strict";
// tslint:disable:max-classes-per-file
Object.defineProperty(exports, "__esModule", { value: true });
const set_1 = require("./set");
class AllTypeSet {
    constructor(setType) {
        this.setType = setType;
        this.type = 'all';
    }
    intersect(other) {
        return other;
    }
    intersectWithSome(other) {
        return other;
    }
    union() {
        return this;
    }
    unionWithSome() {
        return this;
    }
    complement() {
        return new EmptyTypeSet(this.setType);
    }
    isSubsetOf() {
        throw new Error('Not Implemented');
    }
    toJsonSchema() {
        return { type: this.setType };
    }
}
class EmptyTypeSet {
    constructor(setType) {
        this.setType = setType;
        this.type = 'empty';
    }
    intersect() {
        return this;
    }
    intersectWithSome() {
        return this;
    }
    union(other) {
        return other;
    }
    unionWithSome(other) {
        return other;
    }
    complement() {
        return new AllTypeSet(this.setType);
    }
    isSubsetOf() {
        return true;
    }
    toJsonSchema() {
        return false;
    }
}
class SomeTypeSet {
    constructor(setType, subsets) {
        this.setType = setType;
        this.subsets = subsets;
        this.type = 'some';
    }
    static intersectSubsets(subsetListA, subsetListB) {
        let intersectedSubsets = [];
        for (const subsetA of subsetListA) {
            for (const subsetB of subsetListB) {
                const intersectionSubset = subsetA.intersect(subsetB);
                const intersectionTypeSet = exports.createTypeSetFromSubsets(intersectionSubset.setType, [intersectionSubset]);
                const isIntersectionAlreadyRepresented = intersectedSubsets.some((s) => intersectionTypeSet.isSubsetOf(s.typeSet));
                if (isIntersectionAlreadyRepresented) {
                    continue;
                }
                intersectedSubsets = intersectedSubsets.filter((s) => !s.typeSet.isSubsetOf(intersectionTypeSet));
                intersectedSubsets.push({ subset: intersectionSubset, typeSet: intersectionTypeSet });
            }
        }
        return intersectedSubsets.map((s) => s.subset);
    }
    complement() {
        const complementedSubsets = this.subsets
            .map((set) => set.complement())
            .reduce(SomeTypeSet.intersectSubsets);
        return exports.createTypeSetFromSubsets(this.setType, complementedSubsets);
    }
    intersect(other) {
        return other.intersectWithSome(this);
    }
    intersectWithSome(other) {
        const intersectedSubsets = SomeTypeSet.intersectSubsets(this.subsets, other.subsets);
        return exports.createTypeSetFromSubsets(this.setType, intersectedSubsets);
    }
    union(other) {
        return other.unionWithSome(this);
    }
    unionWithSome(other) {
        const newSubsets = [...this.subsets, ...other.subsets];
        return exports.createTypeSetFromSubsets(this.setType, newSubsets);
    }
    isSubsetOf(other) {
        return other.complement().intersect(this).type === 'empty';
    }
    toJsonSchema() {
        return set_1.addAnyOfIfNecessary(this.subsets.map((subset) => subset.toJsonSchema()));
    }
}
const isAnySubsetTypeAll = (subsets) => subsets.some((subset) => subset.type === 'all');
const isTypeAll = (subsets) => {
    return isAnySubsetTypeAll(subsets);
};
const getNonEmptySubsets = (subsets) => subsets.filter((subset) => subset.type !== 'empty');
exports.createTypeSetFromSubsets = (setType, subsets) => {
    const notEmptySubsets = getNonEmptySubsets(subsets);
    if (notEmptySubsets.length === 0) {
        return new EmptyTypeSet(setType);
    }
    if (isTypeAll(subsets)) {
        return new AllTypeSet(setType);
    }
    return new SomeTypeSet(setType, notEmptySubsets);
};
