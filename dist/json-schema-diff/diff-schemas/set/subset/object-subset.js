"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const complement_object_subset_config_1 = require("./object-subset/complement-object-subset-config");
const intersect_object_subset_config_1 = require("./object-subset/intersect-object-subset-config");
const object_subset_config_has_contradictions_1 = require("./object-subset/object-subset-config-has-contradictions");
const object_subset_config_to_json_schema_1 = require("./object-subset/object-subset-config-to-json-schema");
const simplify_object_subset_config_1 = require("./object-subset/simplify-object-subset-config");
const subset_1 = require("./subset");
class SomeObjectSubset {
    constructor(config) {
        this.config = config;
        this.type = 'some';
        this.setType = 'object';
    }
    get properties() {
        return this.config.properties;
    }
    complement() {
        return complement_object_subset_config_1.complementObjectSubsetConfig(this.config).map(exports.createObjectSubsetFromConfig);
    }
    intersect(other) {
        return other.intersectWithSome(this);
    }
    intersectWithSome(other) {
        return exports.createObjectSubsetFromConfig(intersect_object_subset_config_1.intersectObjectSubsetConfig(this.config, other.config));
    }
    toJsonSchema() {
        return object_subset_config_to_json_schema_1.objectSubsetConfigToJsonSchema(this.config);
    }
}
exports.allObjectSubset = new subset_1.AllSubset('object');
exports.emptyObjectSubset = new subset_1.EmptySubset('object');
exports.createObjectSubsetFromConfig = (config) => {
    const simplifiedConfig = simplify_object_subset_config_1.simplifyObjectSubsetConfig(config);
    return object_subset_config_has_contradictions_1.objectSubsetConfigHasContradictions(simplifiedConfig)
        ? exports.emptyObjectSubset
        : new SomeObjectSubset(simplifiedConfig);
};
