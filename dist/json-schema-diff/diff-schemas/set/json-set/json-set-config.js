"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mapJsonSetConfigs = (a, b, callbackFn) => ({
    array: callbackFn(a.array, b.array),
    boolean: callbackFn(a.boolean, b.boolean),
    integer: callbackFn(a.integer, b.integer),
    null: callbackFn(a.null, b.null),
    number: callbackFn(a.number, b.number),
    object: callbackFn(a.object, b.object),
    string: callbackFn(a.string, b.string)
});
exports.intersectJsonSetConfigs = (configA, configB) => mapJsonSetConfigs(configA, configB, (typeSet1, typeSet2) => typeSet1.intersect(typeSet2));
exports.unionJsonSetConfigs = (configA, configB) => mapJsonSetConfigs(configA, configB, (typeSet1, typeSet2) => typeSet1.union(typeSet2));
exports.complementJsonSetConfig = (config) => ({
    array: config.array.complement(),
    boolean: config.boolean.complement(),
    integer: config.integer.complement(),
    null: config.null.complement(),
    number: config.number.complement(),
    object: config.object.complement(),
    string: config.string.complement()
});
