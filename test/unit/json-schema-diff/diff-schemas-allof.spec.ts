import {DiffTestCase, registerDiffTestCases} from '../support/register-diff-test-cases';

describe('diff-schemas allOf', () => {
    const testCases: DiffTestCase[] = [
        {
            description: 'type to equivalent allOf that type',
            examples: ['a', 1],
            input: {
                a: {type: 'string'},
                b: {allOf: [{type: 'string'}]}
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'multiple properties in single schema to equivalent allOf schemas of single properties',
            examples: [
                {}, {first: 'foo'}, {last: 'foo'}, {first: 1}, {last: 1}, {other: 1}, {first: 'foo', last: 'foo'},
                {first: 'foo', last: 1}, {first: 'foo', last: 'foo', other: 1}, 'foo'
            ],
            input: {
                a: {
                    properties: {
                        first: {
                            type: 'string'
                        },
                        last: {
                            type: 'string'
                        }
                    },
                    type: 'object'
                },
                b: {
                    allOf: [
                        {
                            properties: {
                                first: {
                                    type: 'string'
                                }
                            },
                            type: 'object'
                        },
                        {
                            properties: {
                                last: {
                                    type: 'string'
                                }
                            },
                            type: 'object'
                        }
                    ]
                }
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'allOf schemas of different types to equivalent false schema',
            examples: [1, 'foo', true],
            input: {
                a: {
                    allOf: [
                        {type: 'number'},
                        {type: 'string'}
                    ]
                },
                b: false
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'allOf restricting top level types to equivalent schema with the type intersection',
            examples: [1, 'foo', true],
            input: {
                a: {
                    allOf: [
                        {type: 'number'}
                    ],
                    type: ['number', 'string']
                },
                b: {type: 'number'}
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'nested allOf schemas to equivalent single type schema',
            examples: [1, 'foo', true, [], {}],
            input: {
                a: {
                    allOf: [
                        {
                            allOf: [
                                {type: ['number', 'string', 'boolean']},
                                {type: ['number', 'string']}
                            ]
                        },
                        {
                            allOf: [
                                {type: ['number', 'string', 'boolean', 'array']},
                                {type: ['string']}
                            ]
                        }
                    ]
                },
                b: {type: 'string'}
            },
            output: {
                added: false,
                removed: false
            }
        }
    ];

    registerDiffTestCases(testCases);
});
