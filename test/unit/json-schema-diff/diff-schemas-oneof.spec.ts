import {DiffTestCase, registerDiffTestCases} from '../support/register-diff-test-cases';

describe('diff-schemas oneOf', () => {
    const testCases: DiffTestCase[] = [
        {
            description: 'type to equivalent oneOf for that type',
            examples: ['foo', true, 1],
            input: {
                a: {
                    type: 'string'
                },
                b: {
                    oneOf: [
                        {type: 'string'}
                    ]
                }
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'removing the oneOf keyword',
            examples: ['foo', 1, true],
            input: {
                a: {
                    oneOf: [
                        {type: 'string'},
                        {type: 'number'}
                    ]
                },
                b: {
                    type: 'string'
                }
            },
            output: {
                added: false,
                removed: {type: 'number'}
            }
        },
        {
            description: 'adding a disjoint schema to oneOf',
            examples: ['foo', 1, true, {}],
            input: {
                a: {
                    oneOf: [
                        {type: 'string'},
                        {type: 'number'}
                    ]
                },
                b: {
                    oneOf: [
                        {type: 'string'},
                        {type: 'number'},
                        {type: 'boolean'}
                    ]
                }
            },
            output: {
                added: {type: 'boolean'},
                removed: false
            }
        },
        {
            description: 'duplicate oneOf schemas creating a contradiction',
            examples: [1, 'foo'],
            input: {
                a: {
                    oneOf: [
                        {
                            type: 'number'
                        },
                        {
                            type: 'number'
                        }
                    ]
                },
                b: false
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'oneOf containing partial array schemas that are equivalent to all arrays of numbers',
            examples: [[], [1], [1, 2], [1, 2, 3], 'foo'],
            input: {
                a: {
                    oneOf: [
                        {
                            items: {
                                type: 'number'
                            },
                            maxItems: 1,
                            type: 'array'
                        },
                        {
                            items: {
                                type: 'number'
                            },
                            minItems: 2,
                            type: 'array'
                        }
                    ]
                },
                b: {
                    items: {
                        type: 'number'
                    },
                    type: 'array'
                }
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'oneOf containing partial array schemas that partially intersect',
            examples: [[], [1], [1, 2], [1, 2, 3], [1, 2, 3, 4], [1, 2, 3, 4, 5], 'foo'],
            input: {
                a: {
                    oneOf: [
                        {
                            items: {
                                type: 'number'
                            },
                            maxItems: 4,
                            type: 'array'
                        },
                        {
                            items: {
                                type: 'number'
                            },
                            maxItems: 2,
                            type: 'array'
                        }
                    ]
                },
                b: false
            },
            output: {
                added: false,
                removed: {
                    items: {
                        type: 'number'
                    },
                    maxItems: 4,
                    minItems: 3,
                    type: 'array'
                }
            }
        }
    ];

    registerDiffTestCases(testCases);
});
